// const webpack = require("webpack");
const path = require("path");
const fs = require("fs");

const createBuildTemplate = require("template");

const appDirectory = fs.realpathSync(process.cwd());
const resolveAppPath = (relativePath) =>
  path.resolve(appDirectory, relativePath);

module.exports = createBuildTemplate(resolveAppPath, {
  resolve: {
    fallback: {
      buffer: false,
    },
  },
  resolveLoader: {
    alias: {
      "frag-loader": resolveAppPath("src/assets/loaders/frag.loader.js"),
    },
  },
  module: {
    rules: [
      {
        test: /\.frag$/,
        use: ["babel-loader", "frag-loader"],
      },
    ],
  },
});