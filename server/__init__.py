import os
import pkgutil

import template

app = template.Server.create(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../build'))

__import__(__name__, fromlist=[name for _, name, _
                               in pkgutil.walk_packages(__path__)])  # Load all routes
