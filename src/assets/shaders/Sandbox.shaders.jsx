import React from "react";
import { Node } from "gl-react";

import fireShader from "assets/glsl/fire.frag";

const AppSandboxShader = ({ dimensions, time }) => {
  console.log(dimensions);

  // const [mouse, setMouse] = useState([null, null]);

  // useEffect(() => {
  //   const handleMouseMove = (e) =>
  //     setMouse([
  //       e.clientX - e.target.getBoundingClientRect().left,
  //       e.clientY - e.target.getBoundingClientRect().top,
  //     ]);

  //   window.addEventListener("mousemove", handleMouseMove);
  //   return () => window.removeEventListener("mousemove", handleMouseMove);
  // });

  // useEffect(() => console.log(mouse), [mouse]);

  return (
    <Node
      shader={fireShader}
      uniforms={{
        time,
        // mouse
      }}
    />
  );
};

export { AppSandboxShader };
