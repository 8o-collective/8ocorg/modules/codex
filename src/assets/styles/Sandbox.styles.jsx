import styled from "styled-components";

const SandboxContainer = styled.div`
  position: absolute;
  left: 50vw;
  top: 50vw;
  transform: translate(-50%, -50%);

  border: 1px solid red;
`;

export { SandboxContainer };
