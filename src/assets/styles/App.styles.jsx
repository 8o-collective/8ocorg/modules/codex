import styled from "styled-components";

const AppContainer = styled.div`
  background-color: black;
  margin: 0px;
  position: absolute;
  width: 100vw;
  height: 100vh;
  top: 0px;
  left: 0px;

  overflow-y: hidden;
`;

const AppConfigurationText = styled.div`
  position: absolute;
  top: 0;
  right: 0;

  z-index: 1;
  pointer-events: none;

  color: white;
  margin-top: 2%;
  margin-right: 5%;
  text-align: right;

  font-size: 2vh;
  font-family: IBM3270;
`;

export { AppContainer, AppConfigurationText };
