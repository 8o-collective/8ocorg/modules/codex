#define resolution 128.
#define speed 1.
#define turbulence 1.

// uniform vec2 dimensions;
uniform float time;
// uniform vec2 mouse;

in vec2 uv;
out vec4 color;

float rand(vec2 co) {return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);}
float hermite(float t) {return t * t * (3.0 - 2.0 * t);}
float noise(vec2 co, float frequency) {
  vec2 v = vec2(co.x * frequency, co.y * frequency);
  float ix1 = floor(v.x);
  float iy1 = floor(v.y);
  float ix2 = floor(v.x + 1.0);
  float iy2 = floor(v.y + 1.0);
  float fx = hermite(fract(v.x));
  float fy = hermite(fract(v.y));
  float fade1 = mix(rand(vec2(ix1, iy1)), rand(vec2(ix2, iy1)), fx);
  float fade2 = mix(rand(vec2(ix1, iy2)), rand(vec2(ix2, iy2)), fx);
  return mix(fade1, fade2, fy);
}
float pnoise(vec2 co, float freq, int steps, float persistence) {
  float value = 0.0;
  float ampl = 1.0;
  float sum = 0.0;
  for(int i=0 ; i<steps ; i++) {
    sum += ampl;
    value += noise(co, freq) * ampl;
    freq *= 2.0;
    ampl *= persistence;
  }
  return value / sum;
}

void main() {
    vec2 pos = uv;
    pos = floor(pos*resolution)/resolution;
    float gradient = -pos.y - pos.x;
    
    pos -= vec2(0.2, 0.3)*time*speed;
    
    vec4 c1 = vec4(1.2, 0.8, 0.1, 0.25);
    vec4 c4 = vec4(1.0, 0.0, 0.15, 0.0625);
    vec4 c2 = mix(c1, c4, 0.33);
    vec4 c3 = mix(c1, c4, 0.66);

    float noiseTexel = pnoise(pos, 10.0, 5, 0.5)*turbulence;
    
    float t1 = round(smoothstep(-3.0, noiseTexel, gradient));
    float t2 = round(smoothstep(-2.5, noiseTexel, gradient));
    float t3 = round(smoothstep(-2.0, noiseTexel, gradient));
    float t4 = round(smoothstep(-1.5, noiseTexel, gradient));
    
    color = mix(vec4(0.), c4, t1);
    color = mix(color, c3, t2);
    color = mix(color, c2, t3);
    color = mix(color, c1, t4);

    // color = preColor;
}