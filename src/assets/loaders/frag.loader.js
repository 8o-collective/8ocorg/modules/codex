const path = require("path");
const constants = require("../constants.js");

module.exports = function (source) {
  const filename = path.basename(this.resourcePath);
  const assetInfo = { sourceFilename: filename };
  this.emitFile(filename, source, null, assetInfo);

  // const logger = this.getLogger('glsl-loader')
  // logger.info(filename)

  const fragHeader = `
  #version 300 es
  precision highp float;
  precision mediump sampler2D
  #define SIZE ${constants.SIZE - 2}
  `;

  return `
import { Shaders, GLSL } from 'gl-react'

export default Shaders.create({
  shader: {
    frag: GLSL([\`${fragHeader}${source}\`])
  }
}).shader
`;
};

module.exports.raw = true;
