import React, { useState, useEffect } from "react";
import { Surface } from "gl-react-dom";

import { SandboxContainer } from "assets/styles/Sandbox.styles.jsx";

import { AppSandboxShader } from "assets/shaders/Sandbox.shaders.jsx";

import constants from "assets/constants.js";

const Sandbox = () => {
  const [dimension, setDimension] = useState(
    Math.min(window.innerWidth, window.innerHeight)
  );
  const [time, setTime] = useState(0);

  useEffect(() => {
    const handleResize = () =>
      setDimension(Math.min(window.innerWidth, window.innerHeight));

    const interval = setInterval(
      () => setTime((time) => time + constants.TICK_SPEED / 1000),
      constants.TICK_SPEED
    );

    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
      clearInterval(interval);
    };
  });

  // useEffect(() => {
  //   if (time % 1 < constants.TICK_SPEED/1000) game.step()
  // }, [time])

  return (
    <SandboxContainer width={dimension} height={dimension}>
      <Surface width={dimension} height={dimension}>
        <AppSandboxShader dimensions={[dimension, dimension]} time={time} />
      </Surface>
    </SandboxContainer>
  );
};

export default Sandbox;
