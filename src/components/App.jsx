import React from "react";

import {
  AppContainer,
  AppConfigurationText,
} from "assets/styles/App.styles.jsx";

import Sandbox from "components/Sandbox.jsx";

const hash = 12000;

const App = () => {
  return (
    <AppContainer>
      <AppConfigurationText>Configuration: {hash}</AppConfigurationText>
      <Sandbox />
    </AppContainer>
  );
};

export default App;
